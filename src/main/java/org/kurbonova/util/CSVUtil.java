package org.kurbonova.util;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class CSVUtil {
    private static final String CSV_SEPARATOR = ",";

    public static List<String[]> readCSV(String filePath) {
        List<String[]> records = new ArrayList<>();

        try (BufferedReader br = new BufferedReader(new FileReader(filePath))) {
            String line;
            while ((line = br.readLine()) != null) {
                String[] values = line.split(CSV_SEPARATOR);
                records.add(values);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return records;
    }
}