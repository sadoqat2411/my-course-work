package org.kurbonova.controller;

import org.kurbonova.dao.BlenderDAO;
import org.kurbonova.dao.FridgeDAO;
import org.kurbonova.dao.WashingMachineDAO;
import org.kurbonova.model.Blender;
import org.kurbonova.model.Fridge;
import org.kurbonova.model.WashingMachine;
import org.kurbonova.service.BlenderService;
import org.kurbonova.service.FridgeService;
import org.kurbonova.service.WashingMachineService;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Scanner;


public class ProductController {

    private final WashingMachineService washingMachineService;
    private final BlenderService blenderService;
    private final FridgeService fridgeService;
    private Scanner scanner;

    public ProductController() {
        this.washingMachineService = new WashingMachineService(new WashingMachineDAO());
        this.blenderService = new BlenderService(new BlenderDAO());
        this.fridgeService = new FridgeService(new FridgeDAO());
        this.scanner = new Scanner(System.in);
    }


    public void run() {
        System.out.println("HouseHold Appliances WareHouse");
        System.out.println("Sadoqat Kurbonova");
        System.out.println("06/06/2023");
        System.out.println("version 0.2");
        boolean exit = true;
        while (exit) {
            System.out.println("1. Show All Product");
            System.out.println("2. Search");
            System.out.println("0. Exit");
            System.out.print("Enter your choice: ");
            String choice = scanner.nextLine();
            switch (choice) {
                case "1" -> showAllFurniture();
                case "2" -> searchFurniture();
                case "0" -> exit = false;
                default -> System.out.println("Invalid choice. Please try again.");
            }
            System.out.println();
        }
    }

    private void showAllFurniture() {

        boolean running = true;
        scanner = new Scanner(System.in);

        while (running) {
            System.out.println("1. Blender");
            System.out.println("2. Washing Machine");
            System.out.println("3. Fridge");
            System.out.println("0. Exit");
            System.out.print("Enter your choice: ");

            String choice = scanner.nextLine();


            switch (choice) {
                case "1":
                    List<Blender> blenders = blenderService.getAll();
                    if (blenders.isEmpty()) {
                        System.out.println("No washing machines available.");
                    } else {
                        for (Blender blender : blenders) {
                            System.out.println(blender.toString());
                        }
                    }
                    break;
                case "2":
                    List<WashingMachine> washingMachines = washingMachineService.getAll();
                    if (washingMachines.isEmpty()) {
                        System.out.println("No blenders available.");
                    } else {
                        for (WashingMachine washingMachine : washingMachines) {
                            System.out.println(washingMachine.toString());
                        }
                    }
                    break;
                case "3":
                    List<Fridge> fridges = fridgeService.getAll();
                    if (fridges.isEmpty()) {
                        System.out.println("No fridges available.");
                    } else {
                        for (Fridge fridge : fridges) {
                            System.out.println(fridge.toString());
                        }
                    }
                    break;
                case "0":
                    running = false;
                    break;
                default:
                    System.out.println("Invalid choice. Please try again.");
                    break;
            }

        }

    }

    private void searchFurniture() {

        boolean running = true;
        scanner = new Scanner(System.in);

        while (running) {
            System.out.println("1. Search WashingMachine >");
            System.out.println("2. Search Blender Tables >");
            System.out.println("3. Search Fridge >");
            System.out.println("0. Exit");
            System.out.print("Enter your choice: ");

            String choice = scanner.nextLine();

            scanner = new Scanner(System.in);
            switch (choice) {
                case "1" -> {
                    boolean searchRunning = true;
                    while (searchRunning) {
                        System.out.println("1. Search was Name >");
                        System.out.println("2. Search washing machine color >");
                        System.out.println("0. Exit");
                        String search = scanner.nextLine();
                        scanner = new Scanner(System.in);
                        switch (search) {
                            case "1", "2" -> {
                                System.out.println("Enter text");
                                String searchText = scanner.nextLine();
                                filter(searchText);
                            }
                            case "0" -> {
                                searchRunning = false;
                            }
                            default -> {
                                System.out.println("Invalid choice. Please try again.");
                            }
                        }

                    }
                }
                case "2" -> {
                    boolean searchDine = true;
                    while (searchDine) {
                        System.out.println("1. Search By Blender Name >");
                        System.out.println("2. Search By Blender Material >");
                        System.out.println("0. Exit");
                        String search = scanner.nextLine();
                        scanner = new Scanner(System.in);
                        switch (search) {
                            case "1", "2" -> {
                                System.out.println("Enter text");
                                String searchText = scanner.nextLine();
                                filterWashingMachine(searchText);
                            }
                            case "0" -> {
                                searchDine = false;
                            }

                            default -> {
                                System.out.println("Invalid choice. Please try again.");
                            }

                            case "3" -> {
                                boolean searchChine = true;
                                while (searchChine) {
                                    System.out.println("1. Enter Fridge Name >");
                                    System.out.println("2. Enter Fridge Type >");
                                    System.out.println("3. Enter Fridge color >");
                                    System.out.println("0. Exit");
                                    String searchCha = scanner.nextLine();
                                    scanner = new Scanner(System.in);
                                    switch (searchCha) {
                                        case "1", "2", "3" -> {
                                            System.out.println("Enter text");
                                            String searchText = scanner.nextLine();
                                            filterFridge(searchText);
                                        }
                                        case "0" -> {
                                            searchChine = false;
                                        }
                                        default -> {
                                            System.out.println("Invalid choice. Please try again.");
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }


    private void filter(String searchText) {
        List<WashingMachine> washingMachines = washingMachineService.getAll();
        List<WashingMachine> washingMachineList = new ArrayList<>();
        if (washingMachines.isEmpty()) {
            System.out.println("No product available.");
        } else {
            for (WashingMachine washingMachine : washingMachines) {
                if (washingMachine.getName().equalsIgnoreCase(searchText)
                        ||
                        washingMachine.getColor().equalsIgnoreCase(searchText)
                        ||
                        washingMachine.getMaterial().equalsIgnoreCase(searchText)) {
                    washingMachineList.add(washingMachine);
                }
            }

            for (WashingMachine washingMachine : washingMachineList) {
                System.out.println(washingMachine.toString());
            }
        }
    }

    private void filterWashingMachine(String searchText) {
        List<WashingMachine> washingMachines = washingMachineService.getAll();
        List<WashingMachine> washingMachineList = new ArrayList<>();
        if (washingMachines.isEmpty()) {
            System.out.println("No product available.");
        } else {
            for (WashingMachine washingMachine : washingMachines) {
                if (washingMachine.getName().equalsIgnoreCase(searchText)
                        || washingMachine.getColor().equalsIgnoreCase(searchText)
                        || washingMachine.getMaterial().equalsIgnoreCase(searchText)) {
                    washingMachineList.add(washingMachine);
                }
            }

            for (WashingMachine washingMachine : washingMachineList) {
                System.out.println(washingMachine.toString());
            }
        }
    }

    private void filterFridge(String searchChai) {
        List<Fridge> fridges = fridgeService.getAll();
        if (fridges.isEmpty()) {
            System.out.println("No product available.");
        } else {
            List<Fridge> filteredFridge = fridges.stream()
                    .filter(item -> (Objects.nonNull(item.getType()) ? item.getType() : "").contains(searchChai)
                            || (Objects.nonNull(item.getMaterial()) ? item.getMaterial() : "").contains(searchChai) ||
                            (Objects.nonNull(item.getColor()) ? item.getColor() : "").contains(searchChai)).toList();

            for (Fridge fridge : filteredFridge) {
                System.out.println(fridge.toString());
            }
        }
    }

}
