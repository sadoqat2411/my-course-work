package org.kurbonova;

import org.kurbonova.controller.ProductController;

public class Main {
    public static void main(String[] args) {
        ProductController productController = new ProductController();
        productController.run();
    }
}
