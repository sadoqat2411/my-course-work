package org.kurbonova.model;

public class WashingMachine extends Product {
    private String seats;
    private String shape;

    public WashingMachine(int id, String name, String color, String material, String seats, String shape) {
        super(id, name, color, material);
        this.seats = seats;
        this.shape = shape;
    }

    public WashingMachine(int id, String name, String material, String seats, String shape) {
        super(id, name, material);
        this.seats = seats;
        this.shape = shape;
    }

    public WashingMachine(String name, String color, String material, String seats, String shape) {
        super(name, color, material);
        this.seats = seats;
        this.shape = shape;
    }

    public WashingMachine(String name, String material, String seats, String shape) {
        super(name, material);
        this.seats = seats;
        this.shape = shape;
    }

    public String getSeats() {
        return seats;
    }

    public void setSeats(String seats) {
        this.seats = seats;
    }

    public String getShape() {
        return shape;
    }

    public void setShape(String shape) {
        this.shape = shape;
    }

    @Override
    public String toString() {
        return "WashingMachine{" +

                "seats='" + seats + '\'' +
                ", shape='" + shape + '\'' +
                '}';
    }
}
