package org.kurbonova.model;

public class Fridge extends Product {
    private String type;

    public Fridge(String name, String material, String type) {
        super(name, material);
        this.type = type;
    }

    public Fridge(int id, String name, String material, String type) {
        super(id, name, material);
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setMaterial(String updatedMaterial) {

    }

    @Override
    public String toString() {
        return "Fridge{" +
                "type='" + type + '\'' +
                "id=" + super.getId() +
                ", name='" + super.getName() + '\'' +
                ", material='" + super.getMaterial() + '\'' +
                ", color='" + super.getColor() + '\'' +
                '}';
    }
}
