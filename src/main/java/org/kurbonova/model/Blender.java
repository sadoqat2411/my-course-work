package org.kurbonova.model;

public class Blender extends Product {


    public Blender(int id, String name, String color, String material) {
        super(id, name, color, material);
    }


    @Override
    public String toString() {
        return "Blender{" +
                "id=" + super.getId() +
                ", name='" + super.getName() + '\'' +
                ", color='" + super.getColor() + '\'' +
                ", material='" + '\'' + super.getMaterial() +
                '}';
    }
}
