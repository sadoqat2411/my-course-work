package org.kurbonova.model;

public abstract class Product {
    private int id;
    private String name;
    private String material;
    private String color;

    public Product(int id, String name, String color, String material) {
        this.id = id;
        this.name = name;
        this.color = color;
        this.material = material;
    }

    public Product(int id, String name, String material) {
        this.id = id;
        this.name = name;
        this.material = material;
    }

    public Product(String name, String color, String material) {
        this.name = name;
        this.color = color;
        this.material = material;
    }

    public Product(String name, String material) {
        this.name = name;
        this.material = material;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMaterial() {
        return material;
    }

    public void setMaterial(String material) {
        this.material = material;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    @Override
    public String toString() {
        return "Furniture{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", material='" + material + '\'' +
                ", color='" + color + '\'' +
                '}';
    }
}









