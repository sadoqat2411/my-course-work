package org.kurbonova.service;

import org.kurbonova.dao.WashingMachineDAO;
import org.kurbonova.model.WashingMachine;

import java.util.List;

public class WashingMachineService {
    private final WashingMachineDAO washingMachineDAO;

    public WashingMachineService(WashingMachineDAO washingMachineDAO) {
        this.washingMachineDAO = washingMachineDAO;
    }

    public List<WashingMachine> getAll() {
        return washingMachineDAO.getAll();
    }
}