package org.kurbonova.service;

import org.kurbonova.dao.FridgeDAO;
import org.kurbonova.model.Fridge;

import java.util.List;

public class FridgeService {
    private final FridgeDAO fridgeDAO;

    public FridgeService(FridgeDAO fridgeDAO) {
        this.fridgeDAO = fridgeDAO;
    }

    public List<Fridge> getAll() {
        return fridgeDAO.getAll();
    }


}
