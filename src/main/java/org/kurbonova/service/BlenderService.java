package org.kurbonova.service;

import org.kurbonova.dao.BlenderDAO;
import org.kurbonova.model.Blender;

import java.util.List;

public class BlenderService {
    private final BlenderDAO blenderDAO;

    public BlenderService(BlenderDAO blenderDAO) {
        this.blenderDAO = blenderDAO;
    }

    public List<Blender> getAll() {
        return blenderDAO.getAll();
    }


}