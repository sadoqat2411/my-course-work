package org.kurbonova.dao;

import org.kurbonova.model.Fridge;
import org.kurbonova.util.CSVUtil;

import java.util.ArrayList;
import java.util.List;

public class FridgeDAO implements ProductDAO<Fridge> {
    private final List<Fridge> fridges;

    public FridgeDAO() {
        fridges = new ArrayList<>();
        String filePath = "src/main/resources/fridge.csv";
        List<String[]> records = CSVUtil.readCSV(filePath);

        for (String[] record : records) {
            fridges.add(new Fridge(Integer.parseInt(record[0]),
                    record[1],
                    record[2],
                    record[3]));
        }
    }

    @Override
    public List<Fridge> getAll() {
        return fridges;
    }


}