package org.kurbonova.dao;

import org.kurbonova.model.WashingMachine;
import org.kurbonova.util.CSVUtil;

import java.util.ArrayList;
import java.util.List;

public class WashingMachineDAO implements ProductDAO<WashingMachine> {
    private final List<WashingMachine> washingMachines;

    public WashingMachineDAO() {
        washingMachines = new ArrayList<>();
        String filePath = "src/main/resources/washingMachine.csv";
        List<String[]> records = CSVUtil.readCSV(filePath);

        for (String[] record : records) {
            washingMachines.add(new WashingMachine(
                    Integer.parseInt(record[0]),
                    record[1],
                    record[2],
                    record[3],
                    record[4],
                    record[5]));
        }
    }

    @Override
    public List<WashingMachine> getAll() {
        return washingMachines;
    }


}
