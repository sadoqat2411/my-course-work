package org.kurbonova.dao;

import org.kurbonova.model.Blender;
import org.kurbonova.util.CSVUtil;

import java.util.ArrayList;
import java.util.List;

public class BlenderDAO implements ProductDAO<Blender> {
    private final List<Blender> blenders;

    public BlenderDAO() {
        blenders = new ArrayList<>();
        String filePath = "src/main/resources/blender.csv";
        List<String[]> records = CSVUtil.readCSV(filePath);

        for (String[] record : records) {
            Blender blender= new Blender(
                    Integer.parseInt(record[0]),
                    record[1],
                    record[2],
                    record[3]

            );
            blenders.add(blender);
        }
    }

    @Override
    public List<Blender> getAll() {
        return blenders;
    }



}
