package org.kurbonova.dao;

import org.kurbonova.model.Product;

import java.util.List;

public interface ProductDAO<T extends Product> {
    List<T> getAll();
}
